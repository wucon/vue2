export default [
  {
    path: '/home',
    component: () => import(/* webpackChunkName: "page" */ '@/views/home/')
  },
  {
    path: '/three-chart',
    component: () => import(/* webpackChunkName: "page" */ '@/views/three-chart/')
  }
]
